#!/bin/bash

# скачать traceroute


while read LINE; do {
    > $LINE.md
    if (ping -c 5 $LINE)
    then
        ping -c 5 $LINE >> $LINE.md
    elif (traceroute $LINE)
        then
            traceroute $LINE >> $LINE.md
        else
            echo 'NO host' > $LINE.md
    fi
}; done < ip_linux.txt

#https://www.docverter.com/api/
while read LINE; do { 
    curl --form input_files[]=@$LINE.md --form from=markdown --form to=pdf http://c.docverter.com/convert > $LINE.pdf
}; done < ip_linux.txt
